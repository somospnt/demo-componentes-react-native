# demo-componentes-react-native

Proyecto demo de componentes de react native para crear documentación viva y realizar pruebas

## ¿Por que elegir react native? ##
* Un solo codigo que funcione en distintos dispositivos.
* Se puede acceder a funcionalidades propias de cada SO como asi tambien customizar pantallas segun el mismo.
* Navegacion mas fluida y rapida.

## Que se ve en esta demo ##
Expo - Framework para crear aplicaciones  con React Native. Tiene un conjunto de herramientas y servicios sobre React Native para facilitar el desarrollo y build de la aplicación.
Destacan la posibilidad de compilar en la nube la app, sin tener un entorno local. Además, permite ejecutar la aplicación rápido desde de un teléfono.
App.js - Punto de comienzo de la aplicacion, aca es donde se definen las rutas a otras vistas, entre otras cosas.
LinksScreen.js - Pantalla donde se listan los accesos a los distintos componentes. Esto mediante el "navigation" recibido por parametro.
Tambien aca se ve como se crea un componente "OptionButton" al cual se le pasa distintos parametros y es reutilizado por la vista.
ButtonComponentScreen.js - Vista con demo de botones, concatenacion de estilos
WebViewComponentScreen.js - Vista de webview para enbeber paginas de otros sitios
BottomTabNavigatorScreen.js - Vista con tab button y su propia navegacion.