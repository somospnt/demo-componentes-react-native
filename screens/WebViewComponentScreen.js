import * as React from 'react';
import { WebView } from 'react-native-webview';

const informarCookiesScript = `
  setTimeout(function() { 
  window.ReactNativeWebView.postMessage(document.cookie);
  }, 2000);
  true; // note: this is required, or you'll sometimes get silent failures
  `;

//se ejecuta cuando en el web view se ejecuta 
// window.ReactNativeWebView.postMessage
const alRecibirEvento = (event) => {
    console.log(event)
}


export default function WebViewComponentScreen() {
    return (
        <WebView
            source={{
                uri: 'https://github.com/facebook/react-native'
            }}

            //se ejecuta el js dentro del webview
            injectedJavaScript={informarCookiesScript}
            onMessage={alRecibirEvento}
        />
    );
}
